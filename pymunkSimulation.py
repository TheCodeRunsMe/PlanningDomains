import sys, pymunk, pygame, pymunk.pygame_util, math

if len(sys.argv) != 2:
    print("please supply only the max number of boxes")
    sys.exit()

pygame .init()
global windowSize 
windowSize = 800,600
screen = pygame.display.set_mode(windowSize)
pygame.display.set_caption("pymunk tutorial")

clock = pygame.time.Clock()

space = pymunk.Space()
space.gravity = (0, -1000)
#for pymunk renderer: drawOption = pymunk.pygame_util.DrawOptions(screen)

white = 255,255,255
black = 0,0,0
red = 255, 0, 0
green = 0, 255, 0
blue = 0, 0, 255

def addFloor(space):
    fbody = pymunk.Body(body_type = pymunk.Body.STATIC)
    height = 50
    fbody.position = windowSize[0]/2, height
    end1 = -windowSize[0]/2, 0
    end2 = windowSize[0]/2, 0
    fshape = pymunk.Segment(fbody, end1, end2, 5)
    fshape.elasticity = 0.8
    fshape.friction = 0.8
    space.add(fbody, fshape)
    return fshape

def addBox(space, munkCor):
    mass = 1
    inertia = pymunk.moment_for_box(mass, (50,50))
    bbody = pymunk.Body(mass, inertia)
    bbody.position = (munkCor[0], munkCor[1])
    bshape = pymunk.Poly.create_box(bbody, (50,50))
    bshape.elasticity = 0
    bshape.friction = 0.8
    space.add(bbody, bshape)
    return bshape

def addCircle(space, munkCor):
    mass = 1
    inertia = pymunk.moment_for_circle(mass, 0,25)
    cbody = pymunk.Body(mass, inertia)
    cbody.position = munkCor
    cshape = pymunk.Circle(cbody, 25)
    cshape.elasticity = 0
    cshape.friction = 0.8
    space.add(cbody, cshape)
    return cshape

armbody = pymunk.Body(body_type = pymunk.Body.STATIC)
shape1 = pymunk.Circle(armbody, 10)
end1 = 5 * math.sqrt(2.0), -5 * math.sqrt(2.0)
end2 = 15 * math.sqrt(2.0), 15 * math.sqrt(2.0)
shape2 = pymunk.Segment(armbody, end1, end2, 5)
space.add(armbody, shape1, shape2)


def set_position(space):
    cor = pygame.mouse.get_pos()
    mCor = cor[0], windowSize[1] - cor[1]
    armbody.position = mCor
    return cor

line = addFloor(space)
boxes = []
circles = []

while True:
    gameCor = (0, 0)
    for event in pygame.event.get():
        if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:
            sys.exit()
        if len(boxes) == int(sys.argv[1]): #non-optimized method, due to laziness:) 
            pygame.display.set_caption('''Pymunk Tutorial (Caveat! You have reached the maximum number of boxes)''')
        if event.type == pygame.MOUSEBUTTONDOWN and len(boxes) < int(sys.argv[1]):
            gameCor = pygame.mouse.get_pos()
            box = addBox(space, (int(gameCor[0]), int(windowSize[1] - gameCor[1])))
            boxes.append(box)
            print("boxes", len(boxes))
        if event.type == pygame.KEYDOWN and event.key == pygame.K_b:
            gameCor = pygame.mouse.get_pos()
            circle = addCircle(space, (int(gameCor[0]), int(600-gameCor[1])))
            circles.append(circle)
            print("circles", len(circles))
    
    clock.tick(60)
    screen.fill(white)
    space.step(1/60)
   
    arm = set_position(space)
    pygame.draw.line(screen, blue, (arm[0] + 5 * math.sqrt(2), arm[1] + 5 * math.sqrt(2)), (arm[0] + 15 * math.sqrt(2), arm[1] + 15 * math.sqrt(2)), 5)
    pygame.draw.circle(screen, red, arm, 10) 
    
    end1 = pymunk.pygame_util.to_pygame((0,50), screen)
    end2 = pymunk.pygame_util.to_pygame((1000,50), screen)        
    pygame.draw.line(screen, green, (0, 550), (800,550), 5)
    for box in boxes:
        pygame.draw.rect(screen, red, (box.body.position.x - 25, 575-box.body.position.y, 50, 50))
    for circle in circles:
        pos = (int(circle.body.position.x), int(windowSize[1] - circle.body.position.y))
        pygame.draw.circle(screen, blue, pos, 25)
    
    pygame.display.flip()
